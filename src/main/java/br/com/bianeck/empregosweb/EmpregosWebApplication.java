package br.com.bianeck.empregosweb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpregosWebApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpregosWebApplication.class, args);
	}

}
